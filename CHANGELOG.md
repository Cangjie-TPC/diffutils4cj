## 1.0.0

1. 提供比对两组字符串之间差异的功能：
   - 提供差异对比，可以自定义对比内容
2. 提供添加和打包差异补丁的功能
3. 提供以固定格式显示两组字符串差异的功能
